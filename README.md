# <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/8072822/sppfx4.png" alt="icon" width="100"/> sppfx

`sppfx` can run `winetricks`, `winecfg`, `regedit` and arbitrary Windows executables in a SteamPlay prefix, using Proton and the Steam runtime instead of your regular Wine installation.

## Setup
- Check that the default paths to Steam, Proton, your Steam library and the `winetricks` executable are declared correctly at the top of the script -- otherwise you'll have to override them with environment variables every time.

- (Optional) Add game IDs with simple variable names to the script (you can also just call the script with a game ID). Game IDs can be found on the games' Steam store pages -- they're in the URL. See the script for examples!

- Make the script executable:

```
chmod +x sppfx.sh
```

- Make a convenient link to it in your `$PATH`...

```
sudo ln -s /path/to/sppfx.sh /usr/local/bin/sppfx
```
- ...or just `mv` or `cp` it there:

```
sudo mv sppfx.sh /usr/local/bin/sppfx
```

## Usage
- Call the script with the game's ID number or an ID variable you've set in the script as the first argument, and whatever you want to run on the prefix as the remaining arguments. Examples:

```
sppfx war winecfg

sppfx nier winetricks

sppfx 230410 regedit "C:\some-registry-key.reg"

```

- You can optionally use the environment variables `STEAM_LIBRARY_PATH`, `PROTON_PATH`, and `STEAM_PATH`:

```
STEAM_LIBRARY_PATH=/path/to/other/steamlib sppfx 123456 iexplore.exe

PROTON_PATH="/path/to/other/steamlib/steamapps/common/Proton 3.7 Beta" sppfx 123456 winetricks dotnet40

PROTON_PATH="$HOME/.steam/compatibilitytools.d/Proton-3.16-5-GE" sppfx war winecfg 
```

- You can similarly use the environment variable `WINETRICKS_COMMAND` to make `sppfx` use a custom winetricks executable. **Note that the `sppfx` argument needs to be `winetricks` whether or not the executable specified by `WINETRICKS_COMMAND` is in your `$PATH` or called `winetricks`.**

```
WINETRICKS_COMMAND="/path/to/my/winetricks" sppfx 123456 winetricks 

WINETRICKS_COMMAND="w1n3tr1ck5" sppfx 123456 winetricks 
```

- You can prevent sppfx from setting the DLL overrides required for DXVK by setting `SPPFX_NO_DXVK=1`; how much sense this makes or if it's necessary depends on whether or not the last run of the game overwrote the DLLs in the prefix with DXVK's (it does by default).

## Hints
- `sppfx` will insert `wine` automatically if necessary; i.e., the following two commands are equivalent:

```
sppfx 123456 wine "Example Program.exe"
sppfx 123456 "Example Program.exe"
```

- You can make Wine use a one-shot "virtual desktop" without permanently configuring it via `winecfg`, like so:

```
sppfx 123456 explorer /desktop=MyLittleDesktop,1920x1080 "Example Program.exe"
```

- You can enclose paths with spaces/backslashes in `""` (as above) or escape those characters with backslashes:

```
sppfx 123456 C:\\Program\ Files\\Example\ Program.exe
```

- You can use DOS/Windows-style paths (as above) or Unix-style paths:

```
sppfx 123456 "drive_c/Program Files/Internet Explorer/iexplore.exe"
sppfx 123456 drive_c/Program\ Files/Internet\ Explorer/iexplore.exe
```

- Running Windows applications from outside their own directories (as in some of the examples above) *can* lead to errors. If so, `cd` there first or [use `wine start` as described in the Wine user's guide](https://wiki.winehq.org/Wine_User%27s_Guide#Using_wine_start). (The guide tells you to use single quotes or double backslashes in DOS/Windows-style paths, although all of the below variants worked for me --Anjune) 

```
sppfx 123456 start 'C:\Program Files\Internet Explorer\iexplore.exe'
sppfx 123456 start "C:\\Program Files\\Internet Explorer\\iexplore.exe"
sppfx 123456 start "C:\Program Files\Internet Explorer\iexplore.exe"
sppfx 123456 start /unix "drive_c/Program Files/Internet Explorer/iexplore.exe"
```

---
Huge thanks to Anjune for the many improvements!
Credit to Lee Nooks for the original idea.

